package admin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Klasa startowa aplikacji administracyjnej
 */
@SpringBootApplication
@EnableAdminServer
@Slf4j
public class Application {

    public static void main(String[] args) {
        log.info("Podnoszę aplikację administracyjną");
        SpringApplication.run(Application.class, args);
    }
}
