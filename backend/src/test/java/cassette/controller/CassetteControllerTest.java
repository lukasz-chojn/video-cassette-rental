package cassette.controller;

import cassette.dao.CassetteRepo;
import cassette.ds.CassetteDs;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class CassetteControllerTest {

    public static final String DATE = "2020-01-01";

    @Mock
    private CassetteRepo cassetteRepo;
    @Mock
    private CassetteDs cassette;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addCassette() {
        when(cassette.getTitle()).thenReturn("tytuł");
        assertEquals("tytuł", cassette.getTitle());
        cassette.setTitle("tytuł");
        verify(cassette).setTitle("tytuł");

        when(cassette.getDirector()).thenReturn("reżyser");
        assertEquals("reżyser", cassette.getDirector());
        cassette.setDirector("reżyser");
        verify(cassette).setDirector("reżyser");

        when(cassette.getLoanDate()).thenReturn(DATE);
        assertEquals(DATE, cassette.getLoanDate());
        cassette.setLoanDate(DATE);
        verify(cassette).setLoanDate(DATE);

        when(cassette.getReturnDate()).thenReturn(DATE);
        assertEquals(DATE, cassette.getReturnDate());
        cassette.setReturnDate(DATE);
        verify(cassette).setReturnDate(DATE);

        when(cassette.getLoanDays()).thenReturn(0L);
        assertEquals(0L, cassette.getLoanDays());
        cassette.setLoanDays(anyLong());
        verify(cassette).setLoanDays(anyLong());

        when(cassette.getIsLoan()).thenReturn(true);
        assertEquals(true, cassette.getIsLoan());
        cassette.setIsLoan(anyBoolean());
        verify(cassette).setIsLoan(anyBoolean());
    }

    @Test
    void findAll() {
        when(cassetteRepo.findAll()).thenReturn(prepareData());
        assertEquals(2, prepareData().size());
    }

    @Test
    void findAllByTitle() {
        when(cassetteRepo.findAllByTitleContaining("testowy")).thenReturn(prepareData());
        assertEquals(2, prepareData().size());
    }

    @Test
    void updateTitle() {
        cassetteRepo.updateTitle(anyString(), anyString());
        verify(cassetteRepo).updateTitle(anyString(), anyString());
    }

    @Test
    void updateDirector() {
        cassetteRepo.updateDirector(anyString(), anyString());
        verify(cassetteRepo).updateDirector(anyString(), anyString());
    }

    @Test
    void loanCassette() {
        cassetteRepo.loanOrReturn(prepareData().get(0).getTitle(), prepareData().get(0).getDirector(), true);
        verify(cassetteRepo).loanOrReturn(prepareData().get(0).getTitle(), prepareData().get(0).getDirector(), true);

        cassetteRepo.loanDate(prepareData().get(0).getTitle(), prepareData().get(0).getDirector(), DATE);
        verify(cassetteRepo).loanDate(prepareData().get(0).getTitle(), prepareData().get(0).getDirector(), DATE);

        cassetteRepo.returnDate(prepareData().get(0).getTitle(), prepareData().get(0).getDirector(), DATE);
        verify(cassetteRepo).returnDate(prepareData().get(0).getTitle(), prepareData().get(0).getDirector(), DATE);
    }

    @Test
    void deleteCassette() {
        cassetteRepo.deleteByTitleAndDirector(prepareData().get(0).getTitle(), prepareData().get(0).getDirector());
        verify(cassetteRepo).deleteByTitleAndDirector(prepareData().get(0).getTitle(), prepareData().get(0).getDirector());
    }

    private List<CassetteDs> prepareData() {
        CassetteDs cassette1 = new CassetteDs();
        CassetteDs cassette2 = new CassetteDs();

        cassette1.setTitle("tytuł");
        cassette1.setDirector("reżyser");
        cassette1.setLoanDate("");
        cassette1.setReturnDate("");
        cassette1.setLoanDays(0);
        cassette1.setIsLoan(false);

        cassette2.setTitle("testowy film 2");
        cassette2.setDirector("testowy reżyser 2");
        cassette2.setLoanDate("");
        cassette2.setReturnDate("");
        cassette2.setLoanDays(0);
        cassette2.setIsLoan(false);
        return Arrays.asList(cassette1, cassette2);
    }
}