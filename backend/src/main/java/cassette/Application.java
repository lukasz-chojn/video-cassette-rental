package cassette;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Klasa uruchomieniowa aplikacji
 */
@SpringBootApplication
@Slf4j
public class Application {

    public static void main(String[] args) {
        log.info("Uruchamiam aplikację. Klasa startowa " + Application.class.getSimpleName());
        SpringApplication.run(Application.class, args);
    }

}
