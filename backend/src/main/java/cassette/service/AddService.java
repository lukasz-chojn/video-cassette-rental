package cassette.service;

import cassette.constants.RabbitConstants;
import cassette.dao.CassetteRepo;
import cassette.ds.CassetteDs;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Locale;

/**
 * Klasa obsługująca odbiór wiadomości z kolejki
 */
@Service
@Slf4j
public class AddService {

    private ObjectMapper objectMapper;
    private CassetteRepo cassetteRepo;
    private ResourceBundleMessageSource messageSource;

    @RabbitListener(queues = RabbitConstants.RABBITMQ_QUEUE_TO_ADD_DATA)
    public void add(Message message, Channel channel) throws IOException {
        String messageFromQueue = new String(message.getBody());
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        CassetteDs cassetteDs = objectMapper.readValue(messageFromQueue, CassetteDs.class);
        log.info("Odebrana wiadomość " + messageFromQueue);
        cassetteRepo.save(cassetteDs);
        channel.basicAck(deliveryTag, false);

        log.info(messageSource.getMessage("ADD", new Object[]{}, Locale.getDefault()));
        log.debug("Wiadomość z kolejki " + messageFromQueue);
    }

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Autowired
    public void setCassetteRepo(CassetteRepo cassetteRepo) {
        this.cassetteRepo = cassetteRepo;
    }

    @Autowired
    public void setMessageSource(ResourceBundleMessageSource messageSource) {
        this.messageSource = messageSource;
    }
}
