package cassette.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * Parametry wyszukiwania/modyfikacji
 */
@Getter
@Setter
@Slf4j
public class ParamDto {

    private String title;
    private String newTitle;
    private String director;
    private String newDirector;
    private Date loanDate;
    private Date returnDate;
    private boolean isLoan;
}
