package cassette.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Walidator danych przychodzących
 */
@Getter
@Setter
public class CassetteDto {

    @NotNull(message = "Tytuł nie może być pusty")
    private String title;
    @NotNull(message = "Pole z danymi reżysera nie może być puste")
    private String director;
    @NotNull(message = "Data nie może być pusta")
    @DateTimeFormat(pattern = "rrrr-mm-dd")
    private Date loanDate;
}
