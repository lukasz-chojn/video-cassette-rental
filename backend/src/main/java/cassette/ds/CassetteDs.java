package cassette.ds;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Klasa tabeli bazy danych
 */
@Document("cassette")
@Getter
@Setter
@NoArgsConstructor
public class CassetteDs {

    @Id
    @Field("_id")
    @JsonIgnore
    private String id;
    @Indexed(unique = true)
    private String title;
    private String director;
    private String loanDate;
    private String returnDate;
    private long loanDays;
    private Boolean isLoan;

}
