package cassette.scheduler;

import cassette.dao.CassetteRepo;
import cassette.ds.CassetteDs;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Klasa schedulera
 */
@Component
@PropertySource("classpath:properties/scheduler.properties")
@Slf4j
public class Scheduler {

    private CassetteRepo cassetteRepo;
    private MongoOperations mongoOperations;

    @Scheduled(cron = "${cron.scheduler}")
    public void countingDaysScheduler() throws ParseException {

        Update update = new Update();

        Date loanDate;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        LocalDate date = LocalDate.now();
        Date now = dateFormat.parse(date.toString());
        long differenceInMillis;

        List<CassetteDs> cassettes = cassetteRepo.findAll();

        if(cassettes.isEmpty()) {
            log.info("Brak danych do uruchomienia zadania");
            return;
        }

        log.info("Uruchomiono zadanie liczenia dni");

        for (int i = 0; i < cassettes.size(); i++) {
            String loanDateForCassette = cassettes.get(i).getLoanDate();

            if (loanDateForCassette.equals("")) {
                log.info("Tytuł " + cassettes.get(i).getTitle() + " nie zawiera daty wypożyczenia");
            } else {

                loanDate = dateFormat.parse(loanDateForCassette);
                differenceInMillis = now.getTime() - loanDate.getTime();
                long loanDays = TimeUnit.DAYS.convert(differenceInMillis, TimeUnit.MILLISECONDS);

                Query query = new Query(
                        Criteria.where("title").is(cassettes.get(i).getTitle())
                                .andOperator(
                                        Criteria.where("isLoan").is(true)
                                )
                );

                update.set("loanDays", loanDays);

                log.info("Dla tytułu  " + cassettes.get(i).getTitle() + " liczba dni wypożyczenia wynosi " + loanDays + " dzień/dni");
                mongoOperations.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), CassetteDs.class);
                log.info("Zapisano do bazy powyższe informacje");
            }
        }
        log.info("Zakończono zadanie liczenia dni");
    }

    @Autowired
    public void setCassetteRepo(CassetteRepo cassetteRepo) {
        this.cassetteRepo = cassetteRepo;
    }

    @Autowired
    public void setMongoOperations(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }
}
