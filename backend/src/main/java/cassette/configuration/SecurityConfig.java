package cassette.configuration;

import cassette.configuration.handler.UnAuthorizedHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

/**
 * Klasa konfiguracyjna zabezpieczeń
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@Slf4j
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private CorsFilterConfig corsFilterConfig;
    private UnAuthorizedHandler unAuthorizedHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        log.info("Konfiguracja zabezpieczeń");
        http
                .addFilterBefore(corsFilterConfig, ChannelProcessingFilter.class)
                .authorizeRequests()
                .antMatchers("swagger-ui.html", "/login", "/actuator").permitAll()
                .and()
                .formLogin()
                .defaultSuccessUrl("/index.html", true)
                .failureHandler(unAuthorizedHandler)
                .and()
                .csrf().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        log.info("Tworzenie użytkowników");
        auth
                .inMemoryAuthentication()
                .withUser("admin").password(passwordEncoder().encode("admin123")).roles("ADMIN")
                .and()
                .withUser("user").password(passwordEncoder().encode("user123")).roles("USER")
                .and()
                .withUser("guest").password(passwordEncoder().encode("guest123")).roles("ANONYMOUS");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        log.info("Odblokowanie danych statycznych");
        web.ignoring()
                .antMatchers("/css/**", "/js/**", "/webjars/**");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        log.info("Podnoszę enkoder haseł");
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void setCorsFilterConfig(CorsFilterConfig corsFilterConfig) {
        this.corsFilterConfig = corsFilterConfig;
    }

    @Autowired
    public void setUnAuthorizedHandler(UnAuthorizedHandler unAuthorizedHandler) {
        this.unAuthorizedHandler = unAuthorizedHandler;
    }
}
