package cassette.configuration;

import cassette.constants.RabbitConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Klasa konfiguracyjna RabbitMQ
 */
@Configuration
@EnableRabbit
@PropertySource("classpath:properties/rabbit.properties")
@Slf4j
public class RabbitConfig {

    @Value("${rabbitmq.host}")
    private String host;
    @Value("${rabbitmq.port}")
    private Integer port;
    @Value("${rabbitmq.username}")
    private String username;
    @Value("${rabbitmq.password}")
    private String password;

    @Bean
    public ConnectionFactory rabbitConnectionFactoryBean() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        log.info("Konfiguracja bean'a do połączenia z rabbitmq");
        connectionFactory.setHost(host);
        connectionFactory.setPort(port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        return connectionFactory;
    }

    @Bean
    public Jackson2JsonMessageConverter jackson2JsonMessageConverter() {
        log.info("Tworzenie bean'a " + Jackson2JsonMessageConverter.class.getSimpleName());
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpAdmin amqpAdmin() {
        log.info("Tworzenie bean'a " + RabbitAdmin.class.getSimpleName());
        return new RabbitAdmin(rabbitConnectionFactoryBean());
    }

    @Bean
    public Queue queueToAddData() {
        log.info("Tworzenie kolejki do dodawania danych");
        return new Queue(RabbitConstants.RABBITMQ_QUEUE_TO_ADD_DATA, true, false, false);
    }

    @Bean
    public TopicExchange addDataExchange() {
        log.info("Tworzenie kolejki typu " + TopicExchange.class.getSimpleName());
        return new TopicExchange(RabbitConstants.RABBITMQ_EXCHANGE, true, false);
    }

    @Bean
    public Binding binding(Queue queue, TopicExchange exchange) {
        log.info("Łączenie kolejek");
        return BindingBuilder.bind(queue).to(exchange).with(RabbitConstants.RABBITMQ_ROUTING_KEY);
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        log.info("Konfiguracja " + RabbitTemplate.class.getSimpleName());
        RabbitTemplate template = new RabbitTemplate();
        template.setMessageConverter(jackson2JsonMessageConverter());
        template.setEncoding("UTF-8");
        template.setConnectionFactory(rabbitConnectionFactoryBean());
        return template;
    }
}
