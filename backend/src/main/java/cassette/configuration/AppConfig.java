package cassette.configuration;

import com.mongodb.MongoClientURI;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Klasa konfiguracyjna aplikacji
 */
@Configuration
@PropertySource("classpath:properties/db.properties")
@EnableMongoRepositories(basePackages = {"cassette"})
@EnableWebMvc
@Slf4j
public class AppConfig implements WebMvcConfigurer {

    @Value("${urlDatabase}")
    private String urlDatabase;

    @Bean
    public MongoDbFactory mongoDbFactory() {
        log.info("Konfiguracja połączenia z bazą");
        return new SimpleMongoClientDbFactory(String.valueOf(new MongoClientURI(urlDatabase)));
    }

    @Bean
    public ViewResolver viewResolver() {
        log.info("Podnoszę bean'a " + ViewResolver.class.getSimpleName());
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setSuffix(".html");
        viewResolver.setCache(false);
        return viewResolver;
    }

    @Bean
    public ResourceBundleMessageSource resourceBundleMessageSource() {
        log.info("Podnoszę bean'a " + ResourceBundleMessageSource.class.getSimpleName());
        ResourceBundleMessageSource bundleMessageSource = new ResourceBundleMessageSource();
        bundleMessageSource.addBasenames("messages/messages");
        bundleMessageSource.setDefaultEncoding("UTF-8");
        return bundleMessageSource;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("Konfiguracja danych statycznych");
        registry.addResourceHandler("resources/**").addResourceLocations("classpath:resources/");
        registry.addResourceHandler("/**").addResourceLocations("classpath:static/");
        registry.addResourceHandler("css/**").addResourceLocations("classpath:static/css/");
        registry.addResourceHandler("js/**").addResourceLocations("classpath:static/js/");
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
