package cassette.configuration;

import org.springframework.beans.factory.config.PropertyOverrideConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Klasa konfiguracyjna Schedulera
 */
@Configuration
@EnableScheduling
@PropertySource("classpath:properties/scheduler.properties")
@ComponentScan("cassette")
public class SchedulerConfig {

    @Bean
    public PropertyOverrideConfigurer propertyOverrideConfigurer() {
        return new PropertyOverrideConfigurer();
    }
}
