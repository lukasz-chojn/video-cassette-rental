package cassette.dao;

import cassette.ds.CassetteDs;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Główny interfejs aplikacji
 */
@Repository
public interface CassetteRepo extends MongoRepository<CassetteDs, Integer>, CassetteUpdateRepo {

    /**
     * Wyszukiwanie tytułów zawierających słowo z parametru
     * @param title
     * @return lista wszystkich filmów zawierających szukane słowo
     */
    List<CassetteDs> findAllByTitleContaining(String title);

    /**
     * Wyszukiwanie filmów zawierających słowa z parametrów
     * @param title
     * @param director
     * @return lista wszystkich filmów zawierających słowa z parametró
     */
    List<CassetteDs> findByTitleAndDirector(String title, String director);

    /**
     * Usuwanie filmu zawierających słowa z parametrów
     * @param title
     * @param director
     */
    void deleteByTitleAndDirector(String title, String director);
}
