package cassette.dao;

import cassette.ds.CassetteDs;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

/**
 * Implementacja interfejsu CassetteUpdateRepo
 */
@Slf4j
public class CassetteUpdateRepoImpl implements CassetteUpdateRepo {

    private MongoOperations mongoOperations;
    private CassetteRepo cassetteRepo;

    @Override
    public void updateTitle(String oldTitle, String newTitle) {
        Query query = new Query();
        Update update = new Update();

        query.addCriteria(Criteria.where("title").is(oldTitle));
        update.set("title", newTitle);

        log.info("Wykonuję update tytułu filmu");
        mongoOperations.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), CassetteDs.class);
    }

    @Override
    public void updateDirector(String oldName, String newName) {
        Query query = new Query();
        Update update = new Update();

        query.addCriteria(Criteria.where("director").is(oldName));
        update.set("director", newName);

        log.info("Wykonuję update danych reżysera");
        mongoOperations.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), CassetteDs.class);
    }

    @Override
    public void loanOrReturn(String title, String director, boolean isLoan) {
        Query query = new Query();
        Update update = new Update();

        query.addCriteria(Criteria.where("title").is(title)).addCriteria(Criteria.where("director").is(director));

        update.set("isLoan", isLoan);
        log.info("Wykonuję update danych wypożyczenia/zwrotu");
        mongoOperations.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), CassetteDs.class);
    }

    @Override
    public void loanDate(String title, String director, String loanDate) {
        Query query = new Query();
        Update update = new Update();

        query.addCriteria(Criteria.where("title").is(title)).addCriteria(Criteria.where("director").is(director));

        update.set("loanDate", loanDate);
        update.set("returnDate", "");

        log.info("Ustawiam datę wypożyczenia");
        mongoOperations.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), CassetteDs.class);
    }

    @Override
    public void returnDate(String title, String director, String date) {
        Query query = new Query();
        Update update = new Update();

        query.addCriteria(Criteria.where("title").is(title)).addCriteria(Criteria.where("director").is(director));

        update.set("returnDate", date);
        update.set("loanDate", "");
        update.set("loanDays", 0);

        log.info("Ustawiam datę zwrotu");
        mongoOperations.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), CassetteDs.class);
    }

    @Autowired
    public void setMongoOperations(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }
}
