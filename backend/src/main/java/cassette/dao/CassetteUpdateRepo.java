package cassette.dao;

/**
 * Interfejs aktualizujący dane
 */
public interface CassetteUpdateRepo {

    /**
     * aktualizacja tytułu
     * @param oldTitle
     * @param newTitle
     */
    void updateTitle(String oldTitle, String newTitle);

    /**
     * aktualizacja reżysera
     * @param oldName
     * @param newName
     */
    void updateDirector(String oldName, String newName);

    /**
     * ustawienie wypożyczenia/zwrotu
     * @param title
     * @param director
     * @param isLoan
     */
    void loanOrReturn(String title, String director, boolean isLoan);

    /**
     * ustawianie daty wypożyczenia
     * @param title
     * @param director
     * @param date
     */
    void loanDate(String title, String director, String date);

    /**
     * ustawianie daty zwrotu
     * @param title
     * @param director
     * @param date
     */
    void returnDate(String title, String director, String date);
}
