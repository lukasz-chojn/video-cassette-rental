package cassette.constants;

/**
 * Stałe aplikacyjne
 */
public class Constants {
    public static final String HEADER_NAME = "Content-Type";
    public static final String HEADER_VALUE = "text/html; charset=UTF-8";
}
