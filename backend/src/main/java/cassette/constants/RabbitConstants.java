package cassette.constants;

/**
 * Stałe dla RabbitMQ
 */
public class RabbitConstants {
    public static final String RABBITMQ_QUEUE_TO_ADD_DATA="command.addCassette";
    public static final String RABBITMQ_EXCHANGE="addCassette";
    public static final String RABBITMQ_ROUTING_KEY="add";
}
