package cassette.controller;

import cassette.constants.Constants;
import cassette.constants.RabbitConstants;
import cassette.dao.CassetteRepo;
import cassette.ds.CassetteDs;
import cassette.dto.CassetteDto;
import cassette.dto.ParamDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

/**
 * Kontroler do obsługi wypożyczalni
 */
@RestController
@RequestMapping("/cassettes")
@Slf4j
public class CassetteController {

    private CassetteRepo cassetteRepo;
    private ResourceBundleMessageSource messageSource;
    private RabbitTemplate rabbitTemplate;
    private ObjectMapper objectMapper;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> addCassette(@RequestBody @Valid CassetteDto cassetteDto, BindingResult bindingResult) throws JsonProcessingException {

        List<String> errorList = new ArrayList<>();

        CassetteDs cassetteDs = new CassetteDs();

        if (Objects.isNull(cassetteDto)) {
            return null;
        }

        if (bindingResult.hasFieldErrors()) {
            for (ObjectError errors : bindingResult.getAllErrors()) {
                errorList.add(errors.getDefaultMessage());
            }
            log.error(errorList.toString());
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .header(Constants.HEADER_NAME, Constants.HEADER_VALUE)
                    .body(errorList.toString());
        } else {
            cassetteDs.setTitle(cassetteDto.getTitle());
            cassetteDs.setDirector(cassetteDto.getDirector());
            cassetteDs.setLoanDate("");
            cassetteDs.setReturnDate("");
            cassetteDs.setLoanDays(0);
            cassetteDs.setIsLoan(false);

            String jsonToAdd = objectMapper.writeValueAsString(cassetteDs);
            Message message = MessageBuilder
                    .withBody(jsonToAdd.getBytes())
                    .setHeader(SimpMessageHeaderAccessor.MESSAGE_TYPE_HEADER, jsonToAdd)
                    .setContentType(MessageProperties.CONTENT_TYPE_JSON)
                    .build();

            rabbitTemplate.convertAndSend(RabbitConstants.RABBITMQ_QUEUE_TO_ADD_DATA, message);

            log.debug("Wysłano wiadomość " + message);
            log.info(messageSource.getMessage("ADD_TO_QUEUE", new Object[]{cassetteDto.getTitle(), cassetteDto.getDirector()}, Locale.getDefault()));
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header(Constants.HEADER_NAME, Constants.HEADER_VALUE)
                    .body(new Gson().toJson(messageSource.getMessage("ADD_TO_QUEUE", new Object[]{cassetteDto.getTitle(), cassetteDto.getDirector()}, Locale.getDefault())));
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CassetteDs> findAll() {

        if (cassetteRepo.findAll().isEmpty()) {
            return Collections.emptyList();
        }

        return cassetteRepo.findAll();
    }

    @GetMapping(value = "/getBy", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public List<CassetteDs> findAllByTitle(@RequestParam("title") String title) {

        if (cassetteRepo.findAllByTitleContaining(title).isEmpty() || title.equals("")) {
            return Collections.emptyList();
        }

        return cassetteRepo.findAllByTitleContaining(title);
    }

    @PutMapping(value = "/update/title", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> updateTitle(@RequestBody ParamDto paramDto) {

        if (verifyInputData(paramDto) || paramDto.getNewTitle().equals("")) {
            return inputDataError();
        }

        List<CassetteDs> cassettes = cassetteRepo.findByTitleAndDirector(paramDto.getTitle(), paramDto.getDirector());

        if (cassettes.size() > 1) {
            return getError();
        }

        cassetteRepo.updateTitle(cassettes.get(0).getTitle(), paramDto.getNewTitle());

        log.info(messageSource.getMessage("UPDATETITLE", new Object[]{cassettes.get(0).getTitle(), paramDto.getNewTitle()}, Locale.getDefault()));
        return ResponseEntity
                .status(HttpStatus.OK)
                .header(Constants.HEADER_NAME, Constants.HEADER_VALUE)
                .body(new Gson().toJson(messageSource.getMessage("UPDATETITLE", new Object[]{cassettes.get(0).getTitle(), paramDto.getNewTitle()}, Locale.getDefault())));
    }

    @PutMapping(value = "/update/director", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> updateDirector(@RequestBody ParamDto paramDto) {

        if (verifyInputData(paramDto) || paramDto.getNewDirector().equals("")) {
            return inputDataError();
        }

        List<CassetteDs> cassettes = cassetteRepo.findByTitleAndDirector(paramDto.getTitle(), paramDto.getDirector());

        if (cassettes.size() > 1) {
            return getError();
        }

        cassetteRepo.updateDirector(cassettes.get(0).getDirector(), paramDto.getNewDirector());

        log.info(messageSource.getMessage("UPDATEDIRECTOR", new Object[]{cassettes.get(0).getDirector(), paramDto.getNewDirector()}, Locale.getDefault()));
        return ResponseEntity
                .status(HttpStatus.OK)
                .header(Constants.HEADER_NAME, Constants.HEADER_VALUE)
                .body(new Gson().toJson(messageSource.getMessage("UPDATEDIRECTOR", new Object[]{cassettes.get(0).getDirector(), paramDto.getNewDirector()}, Locale.getDefault())));
    }

    @PutMapping(value = "/loanReturn", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> loanCassette(@RequestBody ParamDto paramDto) throws ParseException {

        if (verifyInputData(paramDto)) {
            return inputDataError();
        }

        List<CassetteDs> cassettes = cassetteRepo.findByTitleAndDirector(paramDto.getTitle(), paramDto.getDirector());

        if (cassettes.size() > 1) {
            return getError();
        }

        boolean verifyLoan = cassettes.get(0).getIsLoan();
        LocalDate date = LocalDate.now();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date now = dateFormat.parse(date.toString());
        String formattedDate = dateFormat.format(now);

        if (verifyLoan) {
            cassetteRepo.loanOrReturn(cassettes.get(0).getTitle(), cassettes.get(0).getDirector(), false);
            cassetteRepo.returnDate(cassettes.get(0).getTitle(), cassettes.get(0).getDirector(), formattedDate);
            log.info(messageSource.getMessage("RETURN", new Object[]{cassettes.get(0).getTitle(), cassettes.get(0).getDirector()}, Locale.getDefault()));
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header(Constants.HEADER_NAME, Constants.HEADER_VALUE)
                    .body(new Gson().toJson(messageSource.getMessage("RETURN", new Object[]{cassettes.get(0).getTitle(), cassettes.get(0).getDirector()}, Locale.getDefault())));
        } else {
            cassetteRepo.loanOrReturn(cassettes.get(0).getTitle(), cassettes.get(0).getDirector(), true);
            cassetteRepo.loanDate(cassettes.get(0).getTitle(), cassettes.get(0).getDirector(), formattedDate);
            log.info(messageSource.getMessage("LOAN", new Object[]{cassettes.get(0).getTitle(), cassettes.get(0).getDirector()}, Locale.getDefault()));
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header(Constants.HEADER_NAME, Constants.HEADER_VALUE)
                    .body(new Gson().toJson(messageSource.getMessage("LOAN", new Object[]{cassettes.get(0).getTitle(), cassettes.get(0).getDirector()}, Locale.getDefault())));
        }
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteCassette(@RequestParam("title") String title, @RequestParam("director") String director) {

        if (title.equals("") || director.equals("")) {
            return inputDataError();
        }

        List<CassetteDs> cassettes = cassetteRepo.findByTitleAndDirector(title, director);

        if (cassettes.size() > 1) {
            return getError();
        }

        cassetteRepo.deleteByTitleAndDirector(cassettes.get(0).getTitle(), cassettes.get(0).getDirector());

        log.info(messageSource.getMessage("DELETE", new Object[]{cassettes.get(0).getTitle(), cassettes.get(0).getDirector()}, Locale.getDefault()));
        return ResponseEntity
                .status(HttpStatus.OK)
                .header(Constants.HEADER_NAME, Constants.HEADER_VALUE)
                .body(new Gson().toJson(messageSource.getMessage("DELETE", new Object[]{cassettes.get(0).getTitle(), cassettes.get(0).getDirector()}, Locale.getDefault())));
    }

    private boolean verifyInputData(@RequestBody ParamDto paramDto) {
        return paramDto.getTitle().equals("") || paramDto.getDirector().equals("");
    }

    private ResponseEntity<String> inputDataError() {
        log.error(messageSource.getMessage("INPUT_ERROR", new Object[]{}, Locale.getDefault()));
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header(Constants.HEADER_NAME, Constants.HEADER_VALUE)
                .body(new Gson().toJson(messageSource.getMessage("INPUT_ERROR", new Object[]{}, Locale.getDefault())));
    }

    private ResponseEntity<String> getError() {
        log.error(messageSource.getMessage("ERROR", new Object[]{}, Locale.getDefault()));
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header(Constants.HEADER_NAME, Constants.HEADER_VALUE)
                .body(new Gson().toJson(messageSource.getMessage("ERROR", new Object[]{}, Locale.getDefault())));
    }

    @Autowired
    public void setCassetteRepo(CassetteRepo cassetteRepo) {
        this.cassetteRepo = cassetteRepo;
    }

    @Autowired
    public void setMessageSource(ResourceBundleMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Autowired
    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
}
