Video cassette rental.
Application created for educational purposes. The application is written in Java 8 using Spring Boot, Web, Security.
Data is saved using the RabbitMQ mechanism to the Mongo database. The application contains unit tests