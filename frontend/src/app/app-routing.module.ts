import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from './main/main.component';
import {AddComponent} from './add/add.component';
import {GetComponent} from './get/get.component';
import {ModifyTitleComponent} from './modify-title/modify-title.component';
import {ModifyDirectorComponent} from './modify-director/modify-director.component';
import {DeleteComponent} from './delete/delete.component';
import {LoginComponent} from './login/login.component';

const routes: Routes =
  [
    {
      path: 'home', component: MainComponent
    },
    {
      path: 'login', component: LoginComponent
    },
    {
      path: 'add', component: AddComponent
    },
    {
      path: 'get', component: GetComponent
    },
    {
      path: 'modifyTitle', component: ModifyTitleComponent
    },
    {
      path: 'modifyDirector', component: ModifyDirectorComponent
    },
    {
      path: 'delete', component: DeleteComponent
    }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
