import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AddComponent } from './add/add.component';
import { GetComponent } from './get/get.component';
import { ModifyDirectorComponent } from './modify-director/modify-director.component';
import { ModifyTitleComponent } from './modify-title/modify-title.component';
import { DeleteComponent } from './delete/delete.component';
import { MainComponent } from './main/main.component';
import { AppRoutingModule } from './app-routing.module';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './login/login.component';
import {SecurityService} from './services/security.service';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AddComponent,
    GetComponent,
    ModifyDirectorComponent,
    ModifyTitleComponent,
    DeleteComponent,
    MainComponent,
    MenuComponent,
    LoginComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [SecurityService],
  bootstrap: [MainComponent]
})
export class AppModule { }
