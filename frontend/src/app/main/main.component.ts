import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './main.component.html',
  styleUrls: ['../css/bootstrap.css']
})
export class MainComponent implements OnInit{

  public url = '';
  loggedUser = localStorage.getItem('loggedUser');

  constructor(private router: Router) {

    this.url = this.router.url;
  }

  ngOnInit(): void {
    this.router.navigateByUrl('/home');
  }

}
