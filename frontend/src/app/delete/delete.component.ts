import {Component} from '@angular/core';
import {CassetteService} from '../services/cassette.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['../css/bootstrap.css', '../css/style.css']
})
export class DeleteComponent {
  message: string;
  title: string;
  director: string;

  constructor(private cassette: CassetteService) {
  }


  onSubmit(title: string, director: string) {
    if (title == null || director == null) {
      this.message = 'Tytuł i reżyser to pola obowiązkowe';
      return;
    }
    this.cassette.delete(title, director)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.message;
        });
  }
}
