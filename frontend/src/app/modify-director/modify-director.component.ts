import {Component} from '@angular/core';
import {CassetteService} from '../services/cassette.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-modify-director',
  templateUrl: './modify-director.component.html',
  styleUrls: ['../css/bootstrap.css', '../css/style.css']
})
export class ModifyDirectorComponent {
  message: string;
  title: string;
  director: string;
  newDirector: string;

  constructor(private cassette: CassetteService) { }

  onSubmit(title: string, director: string, newDirector: string) {
    if (title == null || director == null || newDirector == null) {
      this.message = 'Tytuł, reżyser i dane reżysera to pola obowiązkowe';
      return;
    }
    this.cassette.modifyDirector(JSON.stringify({title, director, newDirector}))
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.message;
        }
      );
  }
}
