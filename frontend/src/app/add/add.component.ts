import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';
import {CassetteService} from '../services/cassette.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['../css/bootstrap.css', '../css/style.css']
})
export class AddComponent {
  message: string;
  director: string;
  title: string;

  constructor(private cassette: CassetteService) { }

  onSubmit(title, director) {
    if (title == null || director == null) {
      this.message = 'Tytuł i reżyser to pola obowiązkowe';
      return;
    }
    this.cassette.add(title, director)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.message;
        }
      );
  }
}
