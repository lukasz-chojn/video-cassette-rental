import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  loginLink = 'http://localhost:8080/login';

  constructor(private http: HttpClient) { }

  login(username, password): Observable<any> {
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', '*/*');

    const params = new HttpParams()
      .append('username', username)
      .append('password', password);

    return this.http.post<any>(this.loginLink, params,
      {headers, responseType: 'text' as 'json'});
  }

  logout() {
    localStorage.removeItem('loggedUser');
  }
}
