import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CassetteService {

  mainPartOfLink = 'http://localhost:8080/cassettes';
  getBy = this.mainPartOfLink + '/getBy';
  updateTitle = this.mainPartOfLink + '/update/title';
  updateDirector = this.mainPartOfLink + '/update/director';
  loanOrReturn = this.mainPartOfLink + '/loanReturn';

  httpHeaders = {
    headers: new HttpHeaders()
      .append('Content-Type', 'application/json; charset=UTF-8')
      .append('Accept', '*/*')
  };

  constructor(private http: HttpClient) {
  }

  add(title: string, director: string): Observable<any> {

    const headers = new HttpHeaders()
      .append('Content-Type', 'application/json; charset=UTF-8')
      .append('Accept', '*/*');

    return this.http.post(this.mainPartOfLink, JSON.stringify({title, director}), {headers});
  }

  getAll(): Observable<any> {
    return this.http.get(this.mainPartOfLink, this.httpHeaders);
  }

  getAllByTitle(title: string) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');
    const params = new HttpParams().append('title', title);
    return this.http.get(this.getBy, {headers, params});
  }

  modifyTitle(data: any) {
    return this.http.put(this.updateTitle, data, this.httpHeaders);
  }

  modifyDirector(data: any) {
    return this.http.put(this.updateDirector, data, this.httpHeaders);
  }

  loanReturn(data: any) {
    return this.http.put(this.loanOrReturn, data, this.httpHeaders);
  }

  delete(title: string, director: string) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    const params = new HttpParams().set('title', title).set('director', director);

    return this.http.delete(this.mainPartOfLink, {headers, params});
  }
}
