import {Component} from '@angular/core';
import {SecurityService} from '../services/security.service';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../css/bootstrap.css', '../css/style.css']
})
export class LoginComponent {
  message: string;
  username: string;
  password: string;

  constructor(private security: SecurityService, private router: Router) { }

  onSubmit(username: string, password: string) {
    if (username == null || password == null) {
      this.message = 'Login i hasło to pola obowiązkowe';
      return;
    }
    this.security.login(username, password)
      .subscribe(
        (data: any) => {
          localStorage.setItem('loggedUser', username);
          window.location.reload();
          this.router.navigateByUrl('/home');
        },
        (error: HttpErrorResponse) => {
          this.message = 'Podano błędne dane logowania. Sprawdź dane logowania.';
        }
      );
  }
}
