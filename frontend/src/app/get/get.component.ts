import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CassetteService} from '../services/cassette.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-get',
  templateUrl: './get.component.html',
  styleUrls: ['../css/bootstrap.css', '../css/style.css']
})
export class GetComponent implements OnInit {
  message: string;
  cassettes: [] = [];
  title: string;

  @ViewChild('clear', {static: false}) private clear: ElementRef;
  constructor(private cassette: CassetteService) {
  }

  ngOnInit(): void {
    this.cassette.getAll()
      .subscribe(
        (data: any) => {
          this.cassettes = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.message;
        }
      );
  }

  onSubmit(title: string) {
    if (title == null) {
      this.message = 'Tytuł to pole obowiązkowe';
      return;
    }
    this.cassette.getAllByTitle(title)
      .subscribe(
        (data: any) => {
          this.cassettes = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.message;
        }
      );
  }

  loanOrReturn(title: string, director: string) {
    this.cassette.loanReturn(JSON.stringify({title, director}))
      .subscribe(
        (data: any) => {
          this.message = data;
          this.ngOnInit();
        },
        (error: HttpErrorResponse) => {
          this.message = error.message;
        }
      );
  }
}
