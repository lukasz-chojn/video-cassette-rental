import {Component} from '@angular/core';
import {SecurityService} from '../services/security.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['../css/bootstrap.css']
})
export class MenuComponent {

  loggedUser = localStorage.getItem('loggedUser');

  constructor(private security: SecurityService) { }

  logout() {
    this.security.logout();
    window.location.reload();
  }

}
