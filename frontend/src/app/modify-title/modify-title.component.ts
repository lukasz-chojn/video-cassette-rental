import {Component} from '@angular/core';
import {CassetteService} from '../services/cassette.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-modify-title',
  templateUrl: './modify-title.component.html',
  styleUrls: ['../css/bootstrap.css', '../css/style.css']
})
export class ModifyTitleComponent {
  message: string;
  title: string;
  director: string;
  newTitle: string;

  constructor(private cassette: CassetteService) { }

  onSubmit(title: string, director: string, newTitle: string) {
    if (title == null || director == null || newTitle == null) {
      this.message = 'Tytuł, reżyser i nowy tytuł to pola obowiązkowe';
      return;
    }
    this.cassette.modifyTitle(JSON.stringify({title, director, newTitle}))
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.message;
        }
      );
  }
}
